# Version 0.2.0
## Overview
Changes to the CI/CD pipeline to improve release stability.

## Bug Fix:
- Issue #5 - pipeline will now only release when unit tests are successful. 
Resolved by adding the unit tests as a dependency to the release job.

## New Feature
- Issue #6. Auto-publish releases to packagist. 
Implemented using the Packagist endpoint to request an update. 
This will happen once a release has been made.
