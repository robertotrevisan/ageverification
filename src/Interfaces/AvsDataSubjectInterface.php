<?php
/**
 * Different providers may require different data to be provided, but this will hopefully keep things sane
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package AgeVerification
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\AgeVerification\Interfaces;


interface AvsDataSubjectInterface
{
    public function getFirstName();
    public function getSurname();
    public function getBuilding();
    public function getStreet();
    public function getPostalCode();
    public function getCountry();
    public function getEmail();
    public function getDateOfBirth();
}
