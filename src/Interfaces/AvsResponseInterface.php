<?php
/**
 * The response from the AVS provider
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package AgeVerification
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\AgeVerification\Interfaces;


interface AvsResponseInterface
{
    public function isSuccess(): bool;
    public function getResponseMessage(): string;
}
