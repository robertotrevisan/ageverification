<?php
/**
 * If the details passed to the authenticate() function of AgeChecked, this will be the result
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package AgeVerification
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\AgeVerification\Exception;


class InvalidAgeCheckedCredentialsException extends \Exception
{
    protected $message = 'No secret key has been provided. $credentials array must have the key "secretKey" specified.';
}
