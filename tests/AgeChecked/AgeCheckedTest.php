<?php
/**
 * Unit tests for the AgeCheck class
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package AgeVerification
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\AgeVerification\Tests\AgeChecked;

use GaryBell\AgeVerification\AgeChecked;
use GaryBell\AgeVerification\AgeCheckedDataSubject;
use GaryBell\AgeVerification\Exception\InvalidAgeCheckedCredentialsException;
use GaryBell\AgeVerification\Exception\InvalidPayloadException;
use GaryBell\AgeVerification\GenericDataSubject;
use PHPUnit\Framework\TestCase;

class AgeCheckedTest extends TestCase
{
    public function testAuthenticateWithEmptySecretKey()
    {
        $ageChecked = new AgeChecked();
        $this->expectException(InvalidAgeCheckedCredentialsException::class);
        $ageChecked->authenticate(['secretKey' => '']);
    }

    public function testAuthenticateWithEmptyCredentialsArray()
    {
        $ageChecked = new AgeChecked();
        $this->expectException(InvalidAgeCheckedCredentialsException::class);
        $ageChecked->authenticate([]);
    }

    public function testAuthenticateWithCredentialsArrayWithTypoInSecretKey()
    {
        $ageChecked = new AgeChecked();
        $this->expectException(InvalidAgeCheckedCredentialsException::class);
        $ageChecked->authenticate(['SecretKey' => 'abc123']);
    }

    public function testAuthenticateWithValidSecretKey()
    {
        $ageChecked = new AgeChecked();
        $this->assertTrue($ageChecked->authenticate(['secretKey' => 'abc123']));
    }

    public function testBuildAVSPayloadWithGenericDataSubject()
    {
        $dataSubject = new GenericDataSubject();
        $ageChecked = new AgeChecked();
        $ageChecked->authenticate(['secretKey' => 'abc123']);
        $ageChecked->buildAvsPayload($dataSubject);

        $avsPayload = $ageChecked->getAvsPayload();
        $this->assertIsArray($avsPayload);
        $this->assertArrayHasKey('merchantSecretKey', $avsPayload);
        $this->assertArrayHasKey('name', $avsPayload);
        $this->assertArrayHasKey('surname', $avsPayload);
        $this->assertArrayHasKey('building', $avsPayload);
        $this->assertArrayHasKey('street', $avsPayload);
        $this->assertArrayHasKey('postCode', $avsPayload);
        $this->assertArrayHasKey('countryCode', $avsPayload);
        $this->assertArrayHasKey('email', $avsPayload);
        $this->assertArrayNotHasKey('state', $avsPayload);
        $this->assertArrayNotHasKey('dlicNo', $avsPayload);
    }

    public function testBuildAVSPayloadWithAgeCheckedDataSubjectNoDriversLicense()
    {
        $dataSubject = new AgeCheckedDataSubject();
        $dataSubject->setState('NY');
        $ageChecked = new AgeChecked();
        $ageChecked->authenticate(['secretKey' => 'abc123']);
        $ageChecked->buildAvsPayload($dataSubject);

        $avsPayload = $ageChecked->getAvsPayload();
        $this->assertIsArray($avsPayload);
        $this->assertArrayHasKey('merchantSecretKey', $avsPayload);
        $this->assertArrayHasKey('name', $avsPayload);
        $this->assertArrayHasKey('surname', $avsPayload);
        $this->assertArrayHasKey('building', $avsPayload);
        $this->assertArrayHasKey('street', $avsPayload);
        $this->assertArrayHasKey('postCode', $avsPayload);
        $this->assertArrayHasKey('countryCode', $avsPayload);
        $this->assertArrayHasKey('email', $avsPayload);
        $this->assertArrayHasKey('state', $avsPayload);
        $this->assertArrayNotHasKey('dlicNo', $avsPayload);
        $this->assertEquals('NY', $avsPayload['state']);
    }

    public function testBuildAVSPayloadWithAgeCheckedDataSubject()
    {
        $dataSubject = new AgeCheckedDataSubject();
        $dataSubject->setDriversLicense('test-dl-data');
        $ageChecked = new AgeChecked();
        $ageChecked->authenticate(['secretKey' => 'abc123']);
        $ageChecked->buildAvsPayload($dataSubject);

        $avsPayload = $ageChecked->getAvsPayload();
        $this->assertIsArray($avsPayload);
        $this->assertArrayHasKey('merchantSecretKey', $avsPayload);
        $this->assertArrayHasKey('name', $avsPayload);
        $this->assertArrayHasKey('surname', $avsPayload);
        $this->assertArrayHasKey('building', $avsPayload);
        $this->assertArrayHasKey('street', $avsPayload);
        $this->assertArrayHasKey('postCode', $avsPayload);
        $this->assertArrayHasKey('countryCode', $avsPayload);
        $this->assertArrayHasKey('email', $avsPayload);
        $this->assertArrayNotHasKey('state', $avsPayload);
        $this->assertArrayHasKey('dlicNo', $avsPayload);
        $this->assertEquals('test-dl-data', $avsPayload['dlicNo']);
    }

    public function testEndpointSuffixForNoLicense()
    {
        $dataSubject = new GenericDataSubject();
        $ageChecked = new AgeChecked();
        $ageChecked->buildAvsPayload($dataSubject);
        $this->assertEquals('cceroll', $ageChecked->getEndpointSuffix());
    }

    public function testEndpointSuffixForCheckWithLicense()
    {
        $dataSubject = new AgeCheckedDataSubject();
        $dataSubject->setDriversLicense('test-license');
        $ageChecked = new AgeChecked();
        $ageChecked->buildAvsPayload($dataSubject);
        $this->assertEquals('ccdlic', $ageChecked->getEndpointSuffix());
    }

    public function testPayloadIsValid()
    {
        $ageChecked = new AgeChecked();
        $dataSubject = self::createAgeCheckedDataSubject();

        // test with blank payload
        $this->assertFalse($ageChecked->payloadIsValid());

        // test with no merchantSecretKey set (authenticate hasn't been called)
        $ageChecked->authenticate(['secretKey' => 'abc123']);
        $ageChecked->buildAvsPayload($dataSubject);

        // test with empty first name
        $temporaryDataSubject = clone($dataSubject);
        $temporaryDataSubject->setFirstName('');
        $ageChecked->buildAvsPayload($temporaryDataSubject);
        $this->assertFalse($ageChecked->payloadIsValid());

        // test with empty surname
        $temporaryDataSubject = clone($dataSubject);
        $temporaryDataSubject->setSurname('');
        $ageChecked->buildAvsPayload($temporaryDataSubject);
        $this->assertFalse($ageChecked->payloadIsValid());

        // test with empty building
        $temporaryDataSubject = clone($dataSubject);
        $temporaryDataSubject->setBuilding('');
        $ageChecked->buildAvsPayload($temporaryDataSubject);
        $this->assertFalse($ageChecked->payloadIsValid());

        // test with empty street
        $temporaryDataSubject = clone($dataSubject);
        $temporaryDataSubject->setStreet('');
        $ageChecked->buildAvsPayload($temporaryDataSubject);
        $this->assertFalse($ageChecked->payloadIsValid());

        // test with empty postCode
        $temporaryDataSubject = clone($dataSubject);
        $temporaryDataSubject->setPostalCode('');
        $ageChecked->buildAvsPayload($temporaryDataSubject);
        $this->assertFalse($ageChecked->payloadIsValid());

        // test with empty country code
        $temporaryDataSubject = clone($dataSubject);
        $temporaryDataSubject->setCountry('');
        $ageChecked->buildAvsPayload($temporaryDataSubject);
        $this->assertFalse($ageChecked->payloadIsValid());

        // test with empty email
        $temporaryDataSubject = clone($dataSubject);
        $temporaryDataSubject->setEmail('');
        $ageChecked->buildAvsPayload($temporaryDataSubject);
        $this->assertFalse($ageChecked->payloadIsValid());

        // test with valida payload
        $ageChecked->buildAvsPayload($dataSubject);
        $this->assertTrue($ageChecked->payloadIsValid());

    }

    public function testSubmitWithInvalidPayload()
    {
        $ageChecked = new AgeChecked();
        $this->expectException(InvalidPayloadException::class);
        $ageChecked->submitAgeVerification();
    }

    public function testIsAgeVerifiedDefaultResponse()
    {
        $ageChecked = new AgeChecked();
        $this->assertFalse($ageChecked->isAgeVerified());
    }

    public function testGetRawResponseDefaultResponse()
    {
        $ageChecked = new AgeChecked();
        $this->assertEmpty($ageChecked->getRawResponse());
    }

    public function testGetResponseDefaultResponse()
    {
        $ageChecked = new AgeChecked();
        $this->assertIsArray($ageChecked->getResponse());
        $this->assertEquals(0, sizeof($ageChecked->getResponse()));
    }

    public function testProcessVerificationResponseFailed()
    {
        $rawResponse = self::mockAgeCheckedRawResponse(false);
        $ageChecked = new AgeChecked();
        $ageChecked->processVerificationResponse($rawResponse);
        $this->assertFalse($ageChecked->isAgeVerified());
        $this->assertEquals('Declined', $ageChecked->getResponse()['avstatus']['statusText']);
        $this->assertFalse($ageChecked->getResponse()['authenticated']);
        $this->assertNotEquals(6, $ageChecked->getResponse()['avstatus']['status']);
        $this->assertNotEquals(7, $ageChecked->getResponse()['avstatus']['status']);
        $this->assertNotEquals(13, $ageChecked->getResponse()['avstatus']['status']);
    }

    public function testProcessVerificationResponsePassed()
    {
        $rawResponse = self::mockAgeCheckedRawResponse();
        $ageChecked = new AgeChecked();
        $ageChecked->processVerificationResponse($rawResponse);
        $this->assertTrue($ageChecked->isAgeVerified());
        $this->asserttrue($ageChecked->getResponse()['authenticated']);
        $this->assertEquals('Approved', $ageChecked->getResponse()['avstatus']['statusText']);
        $this->assertNotEquals(8, $ageChecked->getResponse()['avstatus']['status']);
        $this->assertNotEquals(9, $ageChecked->getResponse()['avstatus']['status']);
        $this->assertNotEquals(10, $ageChecked->getResponse()['avstatus']['status']);
        $this->assertNotEquals(11, $ageChecked->getResponse()['avstatus']['status']);
        $this->assertNotEquals(12, $ageChecked->getResponse()['avstatus']['status']);
        $this->assertLessThanOrEqual(13, $ageChecked->getResponse()['avstatus']['status']);
        $this->assertGreaterThan(5, $ageChecked->getResponse()['avstatus']['status']);
    }

    public function testSubmitAgeVerificationConnects()
    {
        $dataSubject = self::createAgeCheckedDataSubject();
        $ageChecked = new AgeChecked();
        $ageChecked->authenticate(['secretKey' => 'willNotValidate']);
        $this->assertTrue(
            $ageChecked->buildAvsPayload($dataSubject)->payloadIsValid()
        );
        $ageChecked->submitAgeVerification();
        $ageChecked->processVerificationResponse($ageChecked->getRawResponse());
        $this->assertNotEmpty($ageChecked->getRawResponse());
        $this->assertNull($ageChecked->getResponse()['authenticated']);
    }

    private static function createAgeCheckedDataSubject(): AgeCheckedDataSubject
    {
        return new AgeCheckedDataSubject(
            'Sherlock',
            'Holmes',
            '221B',
            'Baker Street',
            'W1U 8ED',
            'GB',
            'sherlock@example.com'
        );
    }

    private static function mockAgeCheckedRawResponse(bool $passed = true)
    {
        $passedStatuses = [6, 7, 13];
        $failedStatuses = [5, 8, 9, 10, 11, 14, 15, 16, 17, 18];

        $response = [
            'avstatus' => [
                'agecheckid' => random_int(123456, 765432),
                'ageverifiedid' => md5(time()),
                'status' => $passedStatuses[array_rand($passedStatuses)],
                'statusText' => 'Approved',
                'redirurl' => null,
                'agecheckedpopupurl' => null
            ],
            'authenticated' => true,
            'removemethod' => null,
            'errormessage' => null
        ];

        if (!$passed)
        {
            $response['avstatus']['status'] = $failedStatuses[array_rand($failedStatuses)];
            $response['avstatus']['statusText'] = 'Declined';
            $response['authenticated'] = false;
        }

        return json_encode($response);
    }
}
